﻿using System;
using BackendCodeChallenge.DAL.Entities;

namespace BackendCodeChallenge.DAL
{
    public class UsersRepository : Repository<UserEntity>, IUsersRepository
    {
        public UsersRepository(ApplicationContext context) : base(context)
        {
        }

    }
}
