﻿using System;
using BackendCodeChallenge.DAL;

namespace BackendCodeChallenge.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext _context;

        public IUsersRepository Users { get; private set; }

        public ITasksRepository Tasks { get; private set; }

        public UnitOfWork(ApplicationContext context)
        {
            _context = context;
            Users = new UsersRepository(_context);
            Tasks = new TasksRepository(_context);
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
