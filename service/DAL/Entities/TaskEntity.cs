using System;
using System.ComponentModel.DataAnnotations;

namespace BackendCodeChallenge.DAL.Entities
{
    public class TaskEntity
    {
        [Key]
        public int Id { get; set; }

        public DateTime DueDate { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public int PlannedEffort { get; set; }
        public int? UserId { get; set; }
        public UserEntity User { get; set; }
    }
}
