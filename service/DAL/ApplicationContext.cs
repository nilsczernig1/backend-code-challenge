﻿using System;
using Microsoft.EntityFrameworkCore;
using BackendCodeChallenge.DAL.Entities;

namespace BackendCodeChallenge.DAL
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }

        public DbSet<TaskEntity> Tasks { get; set; }

        public DbSet<UserEntity> Users { get; set; }



    }

}
