﻿using System;
namespace BackendCodeChallenge.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        ITasksRepository Tasks { get; }

        IUsersRepository Users { get; }

        int SaveChanges();
    }
}
