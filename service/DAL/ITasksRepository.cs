﻿using System;
using System.Collections.Generic;
using BackendCodeChallenge.DAL.Entities;

namespace BackendCodeChallenge.DAL
{
    public interface ITasksRepository : IRepository<TaskEntity>
    {
        IEnumerable<TaskEntity> GetTasksDueToday(int size);
    }
}
