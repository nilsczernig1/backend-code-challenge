﻿using System;
using BackendCodeChallenge.DAL.Entities;

namespace BackendCodeChallenge.DAL
{
    public interface IUsersRepository : IRepository<UserEntity>
    {
    }
}
