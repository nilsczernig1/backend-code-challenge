﻿using System;
using System.Collections.Generic;
using System.Linq;
using BackendCodeChallenge.DAL;
using BackendCodeChallenge.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace BackendCodeChallenge.DAL
{
    public class TasksRepository : Repository<TaskEntity>, ITasksRepository
    {

        public TasksRepository(ApplicationContext context) : base(context)
        {
        }

        public IEnumerable<TaskEntity> GetTasksDueToday(int size)
        {
            DateTime today = DateTime.Today;
            return _context.Tasks.Where(x => x.DueDate.Date == today.Date).OrderByDescending(d => d.DueDate).Take(size).ToList();
        }

        public new IEnumerable<TaskEntity> GetAll()
        {
            return _context.Tasks.Include(task => task.User).ToList();
        }

    }
}
