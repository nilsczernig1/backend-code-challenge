﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BackendCodeChallenge.Models
{
    public class User
    {
        public int Id { get; set; }

        [Required]
        [StringLength(25, ErrorMessage = "Please enter a valid First Name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(25, ErrorMessage = "Please enter a valid Last Name")]
        public string LastName { get; set; }
    }
}
