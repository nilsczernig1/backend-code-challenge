using System;
using System.ComponentModel.DataAnnotations;

namespace BackendCodeChallenge.Models
{
    public class TaskPostForm
    {
        public int? UserId { get; set; }
        
        [Display(Name = "Due Date")]
        public DateTime DueDate { get; set; }

        [Required]
        [StringLength(1000, ErrorMessage = "Please enter a valid lenght for the Task Description")]
        public string Description { get; set; }

        [Required]
        [StringLength(250, ErrorMessage = "Please enter a valid Task Name")]
        public string Name { get; set; }

        [Range(0, 100, ErrorMessage = "Please enter valid planned effort")]
        public int PlannedEffort { get; set; }
        public User User { get; set; }
    }
}
