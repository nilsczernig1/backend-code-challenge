﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BackendCodeChallenge.Models
{
    public class TaskPutForm: TaskPostForm
    {
        public int Id { get; set; }
    }
}
