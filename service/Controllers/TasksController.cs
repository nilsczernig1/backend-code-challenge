﻿using System;
using BackendCodeChallenge.DAL;
using Microsoft.AspNetCore.Mvc;
using BackendCodeChallenge.Models;
using BackendCodeChallenge.DAL.Entities;
using System.Collections.Generic;

namespace BackendCodeChallenge.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public TasksController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        [HttpGet]
        [Produces(typeof(List<TaskResult>))]
        public IActionResult Get()
        {
            var tasks = _unitOfWork.Tasks.GetAll();
            return Ok(tasks);
        }

        [HttpGet("{id}")]
        [Produces(typeof(TaskResult))]
        public IActionResult GetById(int id)
        {
            var task = _unitOfWork.Tasks.GetById(id);

            return Ok(task);
        }

        [HttpPost]
        [Produces(typeof(TaskResult))]
        public IActionResult Post(TaskPostForm task)
        {
            UserEntity userEntity;

            if (task.UserId.HasValue)
            {
                userEntity = _unitOfWork.Users.GetById(task.UserId.Value);

                if (userEntity is null)
                {
                    return NotFound($"The User with the {task.UserId} was not found.");
                }
            }

            var taskEntity = new TaskEntity()
            {
                Name = task.Name,
                Description = task.Description,
                UserId = task.UserId ?? null
            };

            _unitOfWork.Tasks.Add(taskEntity);
            _unitOfWork.SaveChanges();

            return Ok(MapTaskResult(taskEntity));
        }

        [HttpPut("{id}")]
        [Produces(typeof(TaskResult))]
        public IActionResult Put(int id, TaskPutForm task)
        {
            if (id != task.Id)
            {
                return BadRequest();
            }

            var user = _unitOfWork.Users.GetById(id);
            var userId = (user is null) ? null : task.UserId;
            var taskEntity = _unitOfWork.Tasks.GetById(id);

            taskEntity.Name = task.Name;
            taskEntity.Description = task.Description;
            taskEntity.DueDate = task.DueDate;
            taskEntity.PlannedEffort = task.PlannedEffort;
            taskEntity.UserId = userId;
            _unitOfWork.Tasks.Update(taskEntity);
            _unitOfWork.SaveChanges();

            return Ok(MapTaskResult(taskEntity));
        }

        private TaskResult MapTaskResult(TaskEntity taskEntity)
        {
            return new TaskResult()
            {
                Description = taskEntity.Description,
                Name = taskEntity.Name,
                Id = taskEntity.Id
            };
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            if (id < 0)
            {
                return NotFound($"The task with the id {id} was not found.");
            }

            var task = _unitOfWork.Tasks.GetById(id);
            _unitOfWork.Tasks.Delete(task);
            _unitOfWork.SaveChanges();
            return NoContent();
        }

        [HttpGet("due-today")]
        public IActionResult GetTasksDueToday(int size = 10)
        {
            var task = _unitOfWork.Tasks.GetTasksDueToday(size);

            return Ok(task);
        }
    }
}
