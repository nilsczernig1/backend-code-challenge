# README #

We have a simple api with in memory database and repository pattern.
This api needs to be improved with some important features.

1. Allow uploading images to a task (max 10 mb) with additional endpoint and the image should be resized to max 100px width.
2. Implement softdelete instead of hard delete of tasks.
3. The Task repository should have a simple auditing of all database changes (logging to console is sufficient, the operation, the changes and the timestamp are important).
4. We have a special usecase - when a header "x-guest: true" is sent to the api, the 'planned effort' should not be returned from any endpoint. Solve this not in the controller directly, but in a generic way, so every endpoint applies this special rule.
